<?php
include_once('../../vendor/autoload.php');
session_start();
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status=$auth->logout();
if($status){
    return Utility::redirect('../../index.php');
}


